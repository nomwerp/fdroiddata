Categories:Reading
License:AGPLv3
Web Site:http://elevenk.tk/portfolio.html
Source Code:https://github.com/jpkrause/DailybRead
Issue Tracker:https://github.com/jpkrause/DailybRead/issues
Donate:https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=RSH5EHTQ8PU8A&lc=US&item_name=Eleven%2dK%20Software&item_number=dailyBreadAndroid&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donate_SM%2egif%3aNonHosted
Bitcoin:1E7nyNHffkzQY6PuZirazUYRFGMhBVeFvZ

Auto Name:DailybRead
Summary:Find books to download and read
Description:
Finds random books for you to read and download from OpenLibrary.

The app features include:

* Find and read random books using the 'Today's bRead' section
* View the book's info and what word was used to find the book
* Save books to your SD card for viewing later
* Search through the entire OpenLibrary database of over 1,000,000 free ebooks

The random search acts as a sort of 'book roulette' by picking random words
from the english language and trying to find a book with that word.
You can see what words it is using for the search while it is looking.
Search as many times as you like to keep finding new, interesting books!
.

Repo Type:git
Repo:https://github.com/jpkrause/DailybRead.git

Build:0.2,5
    commit=V0.2
    subdir=app
    submodules=yes
    gradle=yes
    rm=app/libs/*.jar
    extlibs=epublib/epublib-core-3.1.jar

Maintainer Notes:
Replace epublib with recent srclib version!
.

Auto Update Mode:Version V%v
Update Check Mode:Tags
Current Version:0.2
Current Version Code:5

