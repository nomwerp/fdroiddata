Categories:Science & Education
License:Apache2
Web Site:
Source Code:https://github.com/pires/android-obd-reader
Issue Tracker:https://github.com/pires/android-obd-reader/issues

Summary:Car diagnostics
Description:
OBD II reader, designed to connect to a bluetooth Elm327 OBD reader.
Note application is now called [[com.github.pires.obd.reader]] 
Status: Testing.
.

Repo Type:git
Repo:https://github.com/pires/android-obd-reader.git

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.3
Current Version Code:10

