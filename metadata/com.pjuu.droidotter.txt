Categories:Internet
License:AGPLv3
Web Site:https://pjuu.com/signin
Source Code:https://github.com/pjuu/droidotter/
Issue Tracker:https://github.com/pjuu/droidotter//issues

Auto Name:Pjuu
Summary:Access the Pjuu social network
Description:
Client for the social network [https://pjuu.com/ Pjuu].
.

Repo Type:git
Repo:https://github.com/pjuu/droidotter/

Build:0.2,3
    commit=5b9543d692fae91a211c3ae27dd441a611c92836
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.2
Current Version Code:3

